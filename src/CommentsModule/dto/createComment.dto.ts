import {IsString, IsOptional, IsNumber} from 'class-validator';

export class CreateCommentDto {
    @IsString()
    text: string;

    @IsNumber()
    @IsOptional()
    answerUserId?: number

    @IsNumber()
    @IsOptional()
    parentCommentId?: number

    @IsNumber()
    @IsOptional()
    replyId?: number
}
