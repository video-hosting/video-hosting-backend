import {BadRequestException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {Repository} from "typeorm";
import {AuthService} from "../AuthModule/auth.service";
import {UsersService} from "../UsersModule/users.service";
import {VideoEntity} from "../VideoModule/entities/video.entity";
import {VideoService} from "../VideoModule/video.service";
import {Request} from "express";
import {VideoCommentEntity} from "./entities/videoComment.entity";
import {CreateCommentDto} from "./dto/createComment.dto";
import {prepareChildComment, prepareComment} from "../core/utils/prepareModels";
import {VideoChildCommentEntity} from "./entities/videoChildComment.entity";

@Injectable()
export class CommentsService {
    constructor(
        @InjectRepository(VideoEntity as EntityClassOrSchema)
        private readonly videoRepository: Repository<VideoEntity>,
        @InjectRepository(VideoCommentEntity as EntityClassOrSchema)
        private readonly commentsRepository: Repository<VideoCommentEntity>,
        @InjectRepository(VideoChildCommentEntity as EntityClassOrSchema)
        private readonly childCommentsRepository: Repository<VideoChildCommentEntity>,
        private readonly authService: AuthService,
        private readonly userService: UsersService,
        private readonly videoService: VideoService,
    ) {}

    async getAllComments (videoId: string) {
        return (await this.videoService.getVideoById(+videoId))?.comments
    }

    async addComment (req: Request, createdComment:CreateCommentDto, videoId: string) {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken)
        const user = await this.userService.findUserById(userId, [])
        if (!user) throw new BadRequestException('Пользователь не найден')
        const currentVideo = await this.videoService.getVideoById(+videoId)
        if (!currentVideo) throw new BadRequestException('Видео не найдено')

        if (createdComment.parentCommentId) {
            const parentComm = await this.commentsRepository.findOne({
                where: {id: createdComment.parentCommentId}
            })
            if (!parentComm) throw new BadRequestException('Родительский комментарий не найден')
            const newComm: any = {
                ...createdComment,
                video: currentVideo,
                parentComment: parentComm,
                user
            }
            if (createdComment.answerUserId) {
                const answerUser = await this.userService.findUserById(createdComment.answerUserId)
                if (!answerUser) throw new BadRequestException('Родительский пользователь не найден')
                newComm.answerUser = answerUser
            }
            return await this.childCommentsRepository.save(newComm)
        } else {
            const com = await this.commentsRepository.save({
               ...createdComment,
                video: {id: +videoId},
                user
            })
            return prepareComment(com)
        }
    }

    async getChildrenComments (parentId: string) {
        const res = await this.commentsRepository.findOne({
            where: {id: +parentId}
        })
        return res?.childrenComments?.map(prepareChildComment)
    }
}
