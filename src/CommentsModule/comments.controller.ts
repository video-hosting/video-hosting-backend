import {
    Controller,
    Req,
    Get,
    Param,
    Post,
    Body,
} from '@nestjs/common';
import {Request} from 'express'
import {CommentsService} from "./comments.service";
import {CreateCommentDto} from "./dto/createComment.dto";

@Controller('api/comments')
export class CommentsController {
    constructor(private readonly commentsService: CommentsService) {}
    @Get('list')
    async getAllCommentsForVideo (
        @Param('id') videoId: string
    ) {
        return await this.commentsService.getAllComments(videoId)
    }

    @Post(':id')
    async createComment (
        @Req() req: Request,
        @Param('id') videoId: string,
        @Body() createdComment: CreateCommentDto
    ) {
        return await this.commentsService.addComment(req, createdComment, videoId)
    }

    @Get('children-comments/:id')
    async getChildrenComments (
        @Param('id') parentId: string,
    ) {
        return await this.commentsService.getChildrenComments(parentId)
    }
}
