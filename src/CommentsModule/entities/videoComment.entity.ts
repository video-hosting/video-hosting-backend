import {Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany} from 'typeorm';
import {IsString} from 'class-validator';
import {EntityBase} from "../../core/utils/entity_base";
import {User} from "../../UsersModule/entities/user.entity";
import {VideoEntity} from "../../VideoModule/entities/video.entity";
import {VideoChildCommentEntity} from "./videoChildComment.entity";

@Entity()
export class VideoCommentEntity extends EntityBase {
  @Column()
  @IsString()
  text: string;

  @ManyToOne(() => User, u => u.comments, {
    eager: true,
  })
  user: User;

  @ManyToOne(() => VideoEntity, v => v.comments)
  video: VideoEntity;

  @OneToMany(() => VideoChildCommentEntity, c => c.parentComment, {
    eager: true
  })
  childrenComments: VideoChildCommentEntity[];

  @ManyToMany(() => User, (user) => user.likesComments, {
    eager: true
  })
  @JoinTable({name: "likesComments"})
  likes: User[];

  @ManyToMany(() => User, (user) => user.dislikesComments, {
    eager: true
  })
  @JoinTable({name: "dislikesComments"})
  dislikes: User[];
}
