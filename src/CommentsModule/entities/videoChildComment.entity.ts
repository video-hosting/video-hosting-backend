import {Column, Entity, ManyToOne} from 'typeorm';
import {IsNumber, IsOptional, IsString} from 'class-validator';
import {EntityBase} from "../../core/utils/entity_base";
import {User} from "../../UsersModule/entities/user.entity";
import {VideoCommentEntity} from "./videoComment.entity";

@Entity()
export class VideoChildCommentEntity extends EntityBase {
  @Column()
  @IsString()
  text: string;

  @ManyToOne(() => User, u => u.comments, {
    eager: true,
  })
  user: User;

  @ManyToOne(() => User, u => u.answerComments)
  answerUser: User;

  @ManyToOne(() => VideoCommentEntity, c => c.childrenComments)
  parentComment: VideoCommentEntity;

  @Column({default: null})
  @IsNumber()
  @IsOptional()
  replyId: number
}
