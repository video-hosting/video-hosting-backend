import {Module} from '@nestjs/common';
import {ConfigModule, ConfigService} from "@nestjs/config";
import {TypeOrmModule} from "@nestjs/typeorm";
import {MailerModule} from "@nestjs-modules/mailer";
import configuration from "../../configuration";
import {AuthModule} from "../AuthModule/auth.module";
import {User} from "../UsersModule/entities/user.entity";
import {UsersModule} from "../UsersModule/users.module";
import {MailModule} from "../MailModule/mail.module";
import {ResetPasswordEntity} from "../AuthModule/entities/resetPassword.entity";
import {AvatarModule} from "../AvatarModule/avatar.module";
import {VideoModule} from "../VideoModule/video.module";
import {VideoEntity} from "../VideoModule/entities/video.entity";
import {LikesModule} from "../LikesModule/likes.module";
import {CommentsModule} from "../CommentsModule/comments.module";
import {VideoCommentEntity} from "../CommentsModule/entities/videoComment.entity";
import {VideoChildCommentEntity} from "../CommentsModule/entities/videoChildComment.entity";
import {SubscribesModule} from "../SubscribesModule/subscribes.module";
import {ViewsModule} from "../ViewsModule/views.module";
import {ChannelsModule} from "../ChannelsModule/channels.module";
import {ViewStatsEntity} from "../VideoModule/entities/viewStats.entity";

@Module({
    imports: [
        AuthModule,
        UsersModule,
        ViewsModule,
        MailModule,
        VideoModule,
        LikesModule,
        CommentsModule,
        AvatarModule,
        SubscribesModule,
        ChannelsModule,
        MailerModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (config: ConfigService) => ({
                ...config.get('mailData'),
            }),
            inject: [ConfigService],
        }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (config: ConfigService) => ({
                ...config.get('database'),
                entities: [User, ResetPasswordEntity, VideoEntity, VideoCommentEntity, VideoChildCommentEntity, ViewStatsEntity],
                synchronize: true,
            }),
            inject: [ConfigService],
        }),
        ConfigModule.forRoot({
            load: [configuration],
            envFilePath: ['.env.development'],
        }),],
    controllers: [],
    providers: [],
})
export class AppModule {
}
