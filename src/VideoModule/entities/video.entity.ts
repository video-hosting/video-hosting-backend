import {Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany} from 'typeorm';
import {IsArray, IsBoolean, IsNumber, IsString} from 'class-validator';
import {EntityBase} from "../../core/utils/entity_base";
import {User} from "../../UsersModule/entities/user.entity";
import {VideoCommentEntity} from "../../CommentsModule/entities/videoComment.entity";
import {ViewStatsEntity} from "./viewStats.entity";

@Entity()
export class VideoEntity extends EntityBase {
  @Column()
  @IsString()
  name: string;

  @Column()
  @IsString()
  description: string;

  @Column({type: 'real', default: 0})
  @IsNumber()
  duration: number;

  @Column()
  @IsNumber()
  viewsCount: number;

  @Column()
  @IsNumber()
  height: number;

  @Column()
  @IsNumber()
  width: number;

  @Column({default: false})
  @IsBoolean()
  isPrivate: boolean;

  @Column({default: false})
  @IsBoolean()
  isDeleted: boolean;

  @Column({default: true})
  @IsBoolean()
  isProcessing: boolean;

  @Column({default: true})
  @IsBoolean()
  isModerating: boolean;

  @Column({default: true})
  @IsBoolean()
  isAgeRelatedContent: boolean;

  @Column({ type: 'text', array: true, default: [] })
  @IsArray()
  tags: string[];

  @OneToMany(() => VideoCommentEntity, v => v.video)
  comments: VideoCommentEntity[];

  @OneToMany(() => ViewStatsEntity, v => v.video)
  viewStats: ViewStatsEntity[];

  @ManyToOne(() => User, v => v.videos)
  user: User;

  @ManyToMany(() => User, (user) => user.likes)
  @JoinTable({name: "likes"})
  likes: User[];

  @ManyToMany(() => User, (user) => user.dislikes)
  @JoinTable({name: "dislikes"})
  dislikes: User[];

  @ManyToMany(() => User, (user) => user.views)
  @JoinTable({name: "views"})
  views: User[];
}
