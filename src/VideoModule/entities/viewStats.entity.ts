import {Column, Entity, Index, ManyToOne} from 'typeorm';
import {IsString} from 'class-validator';
import {EntityBase} from "../../core/utils/entity_base";
import {VideoEntity} from "./video.entity";

@Entity()
export class ViewStatsEntity extends EntityBase {
  @Column()
  @IsString()
  ip: string;

  @Column()
  @IsString()
  @Index()
  country: string;

  @ManyToOne(() => VideoEntity, v => v.viewStats)
  video: VideoEntity;
}
