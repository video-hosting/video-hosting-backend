import {IsBoolean, IsString} from 'class-validator';

export class UpdateVideoDto {
    @IsString()
    videoName: string;
    @IsString()
    description: string;
    @IsString()
    tags: string;
    @IsBoolean()
    isPrivate: boolean;
}
