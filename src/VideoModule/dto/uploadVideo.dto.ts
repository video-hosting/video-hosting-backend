import {IsString} from 'class-validator';

export class UploadVideoDto {
    @IsString()
    name: string;
    @IsString()
    description: string;
    @IsString()
    tags: string;
    @IsString()
    isPrivate: 'true' | 'false';
    @IsString()
    isAgeRelatedContent: 'true' | 'false';
}
