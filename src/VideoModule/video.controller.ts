import {
    Controller,
    Post,
    Req,
    UseInterceptors,
    UploadedFile, UseGuards, Body, Res, Get, Param, Delete, Put, Query,
} from '@nestjs/common';
import {FileInterceptor} from "@nestjs/platform-express";
import {Request, Response} from 'express'
import {VideoService} from './video.service';
import {AuthGuard} from "../core/guards/auth.guard";
import {UploadVideoDto} from "./dto/uploadVideo.dto";
import {UpdateVideoDto} from "./dto/updateVideo.dto";

@Controller('api/video')
// @UseGuards(AuthGuard)
export class VideoController {
    constructor(private readonly videoService: VideoService) {
    }

    @Get('list')
    async getAllVideos (
        @Query('q') q?: string
    ) {
        return await this.videoService.getAllVideos(q)
    }

    @Get('moderate-list')
    async getIsModeratingVideos (
        @Req() req: Request,
    ) {
        return await this.videoService.getIsModeratingVideos(req)
    }

    @Put('set-age-control/:id')
    async setAgeControl (
        @Req() req: Request,
        @Param('id') id: string,
        @Body() value: {isAgeRelatedContent: boolean}
    ) {
        return await this.videoService.setAgeControl(req, id, value.isAgeRelatedContent)
    }

    @Put('approve/:id')
    async approveVideo (
        @Req() req: Request,
        @Param('id') id: string,
        @Body() value: {isModerating: boolean}
    ) {
        return await this.videoService.approveVideo(req, id, value.isModerating)
    }

    @Get('related/:id')
    async getRelatedVideos (
        @Param('id') id: string
    ) {
        return await this.videoService.getRelatedVideos(id)
    }

    @Get('/:id')
    async getVideo(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.videoService.getVideoData(id, req)
    }
    @Delete('/:id')
    @UseGuards(AuthGuard)
    async deleteVideo(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.videoService.deleteVideo(id, req)
    }

    @Put('/:id')
    @UseGuards(AuthGuard)
    async updateVideo(
        @Req() req: Request,
        @Param('id') id: string,
        @Body() editData: UpdateVideoDto
    ) {
        return this.videoService.updateVideo(id, editData, req)
    }

    @Get('preview-image/:id')
    async getScreenshot(
        @Param('id') id: string,
        @Res() response: Response,
    ) {
        return this.videoService.getPreviewImage(id, response)
    }

    @Get('file/:id')
    async getVideoFile(
        @Param('id') id: string,
        @Query('quality') quality: string,
        @Res() response: Response,
    ) {
        await this.videoService.getVideoFile(id, response, quality)
    }
    @Post('')
    @UseGuards(AuthGuard)
    @UseInterceptors(FileInterceptor('file'))
    async uploadVideo(
        @UploadedFile() file: Express.Multer.File,
        @Req() req: Request,
        @Res() response: Response,
        @Body() videoDto: UploadVideoDto
    ) {
        await this.videoService.uploadVideo(req, response, videoDto, file);
    }
}
