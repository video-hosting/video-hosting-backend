import {ForbiddenException, Injectable, ServiceUnavailableException} from '@nestjs/common';
import {Request, Response} from 'express'
import {InjectRepository} from "@nestjs/typeorm";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {ArrayContains, FindOptionsWhere, ILike, Repository} from "typeorm";
import * as fs from "fs";
import * as path from "path";
import * as geoip from "geoip-lite";
import {VideoEntity} from "./entities/video.entity";
import {UploadVideoDto} from "./dto/uploadVideo.dto";
import {AuthService} from "../AuthModule/auth.service";
import {
    Qualities,
    Role,
    SAVE_FILES_PATH,
    SCREENSHOTS_NAME,
    VIDEO_FILE_1280_MP4_NAME,
    VIDEO_FILE_1920_MP4_NAME,
    VIDEO_FILE_360_MP4_NAME,
    VIDEO_FILE_480_MP4_NAME,
    VIDEO_FILE_MP4_NAME
} from "../core/constants";
import {CodecInstance} from "../core/utils/CodecInstance";
import {createDirForUserVideo} from "../core/utils/createDirForUserVideo";
import {UsersService} from "../UsersModule/users.service";
import {prepareVideo} from "../core/utils/prepareModels";
import {IVideoResponseWithUserLikes} from "../core/types/IVideoResponse";
import {viewsTimerControl} from "../core/utils/ViewsTimerControl";
import {User} from "../UsersModule/entities/user.entity";
import {UpdateVideoDto} from "./dto/updateVideo.dto";
import {ViewStatsEntity} from "./entities/viewStats.entity";

@Injectable()
export class VideoService {
    constructor(
        @InjectRepository(VideoEntity as EntityClassOrSchema)
        private readonly videoRepository: Repository<VideoEntity>,
        @InjectRepository(ViewStatsEntity as EntityClassOrSchema)
        private readonly viewStatsRepository: Repository<ViewStatsEntity>,
        @InjectRepository(User as EntityClassOrSchema)
        private readonly userRepository: Repository<User>,
        private readonly authService: AuthService,
        private readonly userService: UsersService,
    ) {
    }

    async getAllVideos(q?: string) {
        const defaultFilter =  {
            isModerating: false,
            isPrivate: false,
            isDeleted: false
        }
        return (await this.videoRepository
            .find({
                relations: ['user'],
                order: {
                    createdAt: "DESC"
                },
                ...(!q && {where: defaultFilter}),
                ...(q && {where: [
                        {name: ILike(`%${q}%`), ...defaultFilter},
                        {description: ILike(`%${q}%`), ...defaultFilter},
                        {tags: ArrayContains([q]), ...defaultFilter},
                    ] })
            }))
            .map(v => prepareVideo(v))
    }

    async getIsModeratingVideos(req:Request) {
        const {userId} = this.authService.checkToken(req.headers.authorization as string)
        const user = await this.userService.findUserById(userId, [])
        const isAdmin = user.role.includes(Role.admin)
        if (!isAdmin) {
            throw new ForbiddenException('Данное видео не найдено')
        }

        return (await this.videoRepository
            .find({
                relations: ['user', 'likes', 'dislikes', 'views'],
                where: {
                    isModerating: true,
                }
            }))
            .map(v => prepareVideo(v))
    }

    async setAgeControl(req: Request, id:string, value: boolean) {
        const {userId} = this.authService.checkToken(req.headers.authorization as string)
        const currentVideo = await this.videoRepository.findOneOrFail(
            {where: {id: +id}, relations: ['user']})
        const user = await this.userService.findUserById(userId, [])
        const isAdmin = user.role.includes(Role.admin)
        if (!currentVideo || !isAdmin) {
            throw new ForbiddenException('Данное видео не найдено')
        }
        await this.videoRepository
            .createQueryBuilder()
            .update(VideoEntity)
            .set({isAgeRelatedContent: value})
            .where({id: +id})
            .execute()
        return value
    }

    async approveVideo(req: Request, id:string, value: boolean) {
        const {userId} = this.authService.checkToken(req.headers.authorization as string)
        const currentVideo = await this.videoRepository.findOneOrFail(
            {where: {id: +id}, relations: ['user']})
        const user = await this.userService.findUserById(userId, [])
        const isAdmin = user.role.includes(Role.admin)
        if (!currentVideo || !isAdmin) {
            throw new ForbiddenException('Данное видео не найдено')
        }
        await this.videoRepository
            .createQueryBuilder()
            .update(VideoEntity)
            .set({isModerating: value})
            .where({id: +id})
            .execute()
        return true
    }

    async getRelatedVideos(videoId: string) {
        const currentVideo = await this.videoRepository.findOne({
            where: {id: +videoId},
            relations: ['user']
        })
        if (!currentVideo?.user) return []
        const videos:VideoEntity[] = await this.userRepository
            .createQueryBuilder()
            .relation(User, 'videos')
            .of(currentVideo?.user)
            .loadMany()
        return videos.filter(v => v.id !== +videoId && !v.isModerating && !v.isPrivate && !v.isDeleted)
    }

    async getVideoData(id: string, req?: Request): Promise<IVideoResponseWithUserLikes> {
        let isLikedVideoCurrentUser: boolean = false
        let isDislikedVideoCurrentUser: boolean = false
        let user
        const video = await this.videoRepository.findOne({
            where: {id: +id},
            relations: ['user', 'comments', 'likes', 'dislikes']
        })
        if (req && req.headers.authorization) {
            try {
                const {userId} = this.authService.checkToken(req.headers.authorization as string)
                user = await this.userService.findUserById(userId, ['likes', 'dislikes'])
                isLikedVideoCurrentUser = !!(await this.userRepository
                    .createQueryBuilder('user')
                    .where({id: userId})
                    .leftJoinAndSelect('user.likes', 'likes')
                    .where('likes.id =:id', {id: +id})
                    .getOne())
                isDislikedVideoCurrentUser = !!(await this.userRepository
                    .createQueryBuilder('user')
                    .where({id: userId})
                    .leftJoinAndSelect('user.dislikes', 'dislikes')
                    .where('dislikes.id =:id', {id: +id})
                    .getOne())
                await this.userRepository
                    .createQueryBuilder()
                    .relation(User, 'views')
                    .of(user)
                    .remove(+id)
                await this.userRepository
                    .createQueryBuilder()
                    .relation(User, 'views')
                    .of(user)
                    .add(+id)
            } catch (err) {
            }
        }
        if (req) {
            const ip = req?.headers['x-forwarded-for'] || req?.socket.remoteAddress
            if (viewsTimerControl.checkTimer(ip, +id)) {
                viewsTimerControl.setTimer(ip, +id)
                await this.videoRepository.save({...video, viewsCount: (video?.viewsCount || 0) + 1})
                await this.viewStatsRepository.save({
                    ip: ip as string,
                    country: String(geoip.lookup(ip as string)),
                    video: video as VideoEntity
                })
            }
        }
        let subscribersCount = 0
        if (video) {
            subscribersCount = (await this.userService
                .findUserById(video?.user.id, ['subscribers'])).subscribers.length as number
        }
        const preparedData = prepareVideo(video as VideoEntity, user)
        return {
            ...preparedData,
            user: {
                ...preparedData.user,
                subscribersCount
            },
            ...(isLikedVideoCurrentUser && {isLikedVideo: isLikedVideoCurrentUser}),
            ...(isDislikedVideoCurrentUser && {isDislikedVideo: isDislikedVideoCurrentUser})
        }
    }

    async getVideoById(id: number) {
        return this.videoRepository.findOne({
            where: {id: +id},
            relations: ['user', 'likes', 'dislikes', 'comments', 'views']
        })
    }

    async getVideoFile(id: string, response: Response, quality: string) {
        const videoData = await this.getVideoData(id)
        const userId = videoData?.user.id
        response.set({
            'Content-Type': 'video/mp4',
            'Content-Disposition': `attachment; filename="sample-5s.mp4"`,
        });
        if (quality === Qualities.p1080) {
            response.sendFile(path.resolve(SAVE_FILES_PATH, (userId as number).toString(), 'videos', id, VIDEO_FILE_1920_MP4_NAME))
        } else if (quality === Qualities.p720) {
            response.sendFile(path.resolve(SAVE_FILES_PATH, (userId as number).toString(), 'videos', id, VIDEO_FILE_1280_MP4_NAME))
        } else if (quality === Qualities.p480) {
            response.sendFile(path.resolve(SAVE_FILES_PATH, (userId as number).toString(), 'videos', id, VIDEO_FILE_480_MP4_NAME))
        } else {
            response.sendFile(path.resolve(SAVE_FILES_PATH, (userId as number).toString(), 'videos', id, VIDEO_FILE_360_MP4_NAME))
        }
    }

    async uploadVideo(
        req: Request,
        response: Response,
        videoDto: UploadVideoDto,
        file: Express.Multer.File
    ): Promise<any> {
        try {
            const accessToken = req.headers.authorization as string;
            const {userId} = this.authService.checkToken(accessToken)
            const user = await this.userService.findUserById(userId)

            const newVideo = await this.videoRepository.save({
                user,
                name: videoDto.name,
                description: videoDto.description,
                tags: videoDto.tags.split(',').filter(t => !!t),
                isPrivate: videoDto.isPrivate === 'true',
                isProcessing: true,
                isModerating: true,
                isAgeRelatedContent: videoDto.isAgeRelatedContent === 'true',
                viewsCount: 0,
                duration: 0,
                height: 0,
                width: 0,
            })

            createDirForUserVideo(userId, newVideo.id.toString())

            const filePath = path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos', newVideo.id.toString(), VIDEO_FILE_MP4_NAME)

            try {
                fs.writeFileSync(filePath, file.buffer)
            } catch (err) {
                throw new ServiceUnavailableException('Не удалось сохранить файл')
            }

            const videoData = await CodecInstance.getCodecInfo(filePath)
            if (!videoData) {
                throw new ServiceUnavailableException('Нет информации о кодеке')
            }
            const res = await this.videoRepository.save({
                ...newVideo,
                duration: videoData.duration,
                width: videoData.width,
                height: videoData.height,
            })
            response.status(201)
            response.json(res)
            response.end()
            setTimeout(async () => {
                await new CodecInstance(filePath).saveFullHDVideo(userId, res.id.toString())
                await new CodecInstance(filePath).saveHDVideo(userId, res.id.toString())
                await new CodecInstance(filePath).save480pVideo(userId, res.id.toString())
                await new CodecInstance(filePath).save360pVideo(userId, res.id.toString())
                await new CodecInstance(filePath).saveScreenshots(userId, res.id.toString())

                fs.unlinkSync(filePath)
                await this.videoRepository.save({
                    id: res.id,
                    isProcessing: false,
                })
            }, 0)
        } catch (err) {
            response.status(500)
            response.json(err.response)
            response.end()
        }
    }

    async getPreviewImage(id: string, response: Response) {
        const videoData = await this.videoRepository.findOne({where: {id: +id}, relations: ['user']})
        const userId = videoData?.user.id
        const filePath = path.resolve(SAVE_FILES_PATH, (userId as number).toString(), 'videos', id, SCREENSHOTS_NAME, 'tn_1.webp')
        const isHaveFile = fs.existsSync(filePath)
        if (isHaveFile) {

            response.set({
                'Content-Type': 'image/webp',
                'Content-Disposition': `attachment; filename="tn_1.webp"`,
                'Cache-Control': 'private, max-age=3600'
            });
            response.sendFile(filePath)
        } else {
            const defaultPath = path.resolve('src', 'core', 'images', 'defaultScreen.webp')
            response.set({
                'Content-Type': 'image/webp',
                'Content-Disposition': `attachment; filename="defaultScreen.webp"`,
                'Cache-Control': 'private, max-age=0'
            });
            response.sendFile(defaultPath)
        }
    }

    async deleteVideo(id: string, req: Request) {
        try {
            if (req.headers.authorization) {
                const {userId} = this.authService.checkToken(req.headers.authorization as string)
                const currentVideo = await this.videoRepository.findOneOrFail(
                    {where: {id: +id}, relations: ['user']})
                if (!currentVideo || userId !== currentVideo.user.id) {
                    throw new ForbiddenException('Данное видео не найдено')
                }
                await this.videoRepository
                    .createQueryBuilder()
                    .update(VideoEntity)
                    .set({isDeleted: true})
                    .where({id: +id})
                    .execute()
                return true
            } else {
                throw new ForbiddenException('Вы не можете удалить данное видео')
            }
        } catch (err) {
            throw new ForbiddenException('Вы не можете удалить данное видео')
        }
    }

    async updateVideo(id: string, editData: UpdateVideoDto, req: Request) {
        try {
            const {userId} = this.authService.checkToken(req.headers.authorization as string)
            const currentVideo = await this.videoRepository.findOneOrFail(
                {where: {id: +id}, relations: ['user']})
            if (!currentVideo || userId !== currentVideo.user.id) {
                throw new ForbiddenException('Данное видео не найдено')
            }
            await this.videoRepository
                .createQueryBuilder()
                .update(VideoEntity)
                .set({
                    name: editData.videoName,
                    description: editData.description,
                    isPrivate: editData.isPrivate,
                    tags: editData.tags.split(',').filter(t => !!t)
                })
                .where({id: +id})
                .execute()
            return true
        } catch (err) {
            throw new ForbiddenException('Вы не можете изменить данное видео')
        }
    }
}
