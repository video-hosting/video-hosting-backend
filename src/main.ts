import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import * as cookieParser from 'cookie-parser';
import * as compression from 'compression';
import { AppModule } from './AppModule/app.module';
import {TypeOrmUniqueViolationInterceptor} from "./core/utils/typeorm-unique-violation.interceptor";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get<any, any>(ConfigService);
  app.useGlobalInterceptors(new TypeOrmUniqueViolationInterceptor());
  app.enableCors({
    origin: [`${configService.get('clientUrl')}`],
    credentials: true,
  });
  app.use(compression());
  app.use(cookieParser());

  await app.listen(configService.get('port'));
}
bootstrap();
