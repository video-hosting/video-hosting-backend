import {BadRequestException, Injectable, ServiceUnavailableException} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {Repository} from "typeorm";
import {AuthService} from "../AuthModule/auth.service";
import {UsersService} from "../UsersModule/users.service";
import {VideoEntity} from "../VideoModule/entities/video.entity";
import {VideoService} from "../VideoModule/video.service";
import {Request} from "express";
import {ILikesChanged} from "../core/types/IVideoResponse";
import {VideoCommentEntity} from "../CommentsModule/entities/videoComment.entity";

@Injectable()
export class LikesService {
    constructor(
        @InjectRepository(VideoEntity as EntityClassOrSchema)
        private readonly videoRepository: Repository<VideoEntity>,
        @InjectRepository(VideoCommentEntity as EntityClassOrSchema)
        private readonly commentsRepository: Repository<VideoCommentEntity>,
        private readonly authService: AuthService,
        private readonly userService: UsersService,
        private readonly videoService: VideoService,
    ) {}

    async checkIsDisliked(userId: number, videoId: number) {
        try {
            return !!(await this.videoRepository
                .createQueryBuilder('video')
                .where({id: videoId})
                .leftJoinAndSelect('video.dislikes', 'dislikes')
                .where('dislikes.id =:id', {id: userId})
                .getOne())
        } catch (err) {
            return false
        }
    }

    async checkIsLiked(userId: number, videoId: number) {
        try {
            return !!(await this.videoRepository
                .createQueryBuilder('video')
                .where({id: videoId})
                .leftJoinAndSelect('video.likes', 'likes')
                .where('likes.id =:id', {id: userId})
                .getOne())
        } catch (err) {
            return false
        }
    }

    async checkIsLikedOrDislikedComment(userId: number, commentId: number, type: 'likes' | 'dislikes') {
        try {
            return !!(await this.commentsRepository
                .createQueryBuilder('comment')
                .where({id: commentId})
                .leftJoinAndSelect(`comment.${type}`, type)
                .where(`${type}.id =:id`, {id: userId})
                .getOne())
        } catch (err) {
            return false
        }
    }

    async addLikeOrDislikeCommentFromDB(comment: VideoCommentEntity, userId: number, type: 'likes' | 'dislikes') {
        try {
            return await this.commentsRepository
                .createQueryBuilder()
                .relation(VideoCommentEntity,type)
                .of(comment)
                .add(userId)
        } catch (err) {console.log(err)}
    }
    async deleteLikeOrDislikeCommentFromDB(comment: VideoCommentEntity, userId: number, type: 'likes' | 'dislikes') {
        try {
            return await this.commentsRepository
                .createQueryBuilder()
                .relation(VideoCommentEntity,type)
                .of(comment)
                .remove(userId)
        } catch (err) {console.log(err)}
    }


    async addLikeOrDislikeFromDB(video: VideoEntity, userId: number, type: 'likes' | 'dislikes') {
        try {
            return await this.videoRepository
                .createQueryBuilder()
                .relation(VideoEntity,type)
                .of(video)
                .add(userId)
        } catch (err) {console.log(err)}
    }
    async deleteLikeOrDislikeFromDB(video: VideoEntity, userId: number, type: 'likes' | 'dislikes') {
        try {
            return await this.videoRepository
                .createQueryBuilder()
                .relation(VideoEntity,type)
                .of(video)
                .remove(userId)
        } catch (err) {console.log(err)}
    }

    async likeVideo(id: string, req: Request): Promise<ILikesChanged> {
        try {
            const accessToken = req.headers.authorization as string;
            const {userId} = this.authService.checkToken(accessToken)
            const currentVideo = await this.videoRepository.findOne({
                where: {id: +id},
            })
            if (!currentVideo) {throw new Error('Нет видео')}
            const isDisliked = await this.checkIsDisliked(userId, +id)
            if (isDisliked) {
                await this.deleteLikeOrDislikeFromDB(currentVideo, userId, 'dislikes')
            }
            await this.addLikeOrDislikeFromDB(currentVideo, userId, 'likes')
            const savedVideo = await this.videoRepository.findOne({
                where: {id: +id},
                relations: ['likes', 'dislikes']
            })
            return {
                likes: savedVideo?.likes.length || 0,
                dislikes: savedVideo?.dislikes.length || 0,
                isLiked: true,
                isDisliked: false
            }
        } catch (err) {
            throw new ServiceUnavailableException('Что-то пошло не так')
        }
    }

    async dislikeVideo(id: string, req: Request): Promise<ILikesChanged> {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken)
        const currentVideo = await this.videoService.getVideoById(+id)

        if (!currentVideo) {
            throw new BadRequestException('Нет видео')
        }
        const isLiked = await this.checkIsLiked(userId, +id)

        if (isLiked) {
            await this.deleteLikeOrDislikeFromDB(currentVideo, userId, 'likes')
        }
        await this.addLikeOrDislikeFromDB(currentVideo, userId, 'dislikes')
        const savedVideo = await this.videoRepository.findOne({
            where: {id: +id},
            relations: ['likes', 'dislikes']
        })
        return {
            likes: savedVideo?.likes.length || 0,
            dislikes: savedVideo?.dislikes.length || 0,
            isLiked: false,
            isDisliked: true
        }
    }

    async deleteLikeVideo(id: string, req: Request): Promise<ILikesChanged> {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken)
        const currentVideo = await this.videoRepository.findOne({where: {id: +id}})

        if (!currentVideo) {throw new BadRequestException('Нет видео')}

        await this.deleteLikeOrDislikeFromDB(currentVideo, userId, 'likes')
        const savedVideo = await this.videoRepository.findOne({
            where: {id: +id},
            relations: ['likes', 'dislikes']
        })
        return {
            likes: savedVideo?.likes.length || 0,
            dislikes: savedVideo?.dislikes.length || 0,
            isLiked: false,
            isDisliked: false
        }
    }

    async deleteDislikeVideo(id: string, req: Request): Promise<ILikesChanged> {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken)
        const currentVideo = await this.videoRepository.findOne({where: {id: +id}})

        if (!currentVideo) {throw new BadRequestException('Нет видео')}

        await this.deleteLikeOrDislikeFromDB(currentVideo, userId, 'dislikes')

        const savedVideo = await this.videoRepository.findOne({
            where: {id: +id},
            relations: ['likes', 'dislikes']
        })
        return {
            likes: savedVideo?.likes.length || 0,
            dislikes: savedVideo?.dislikes.length || 0,
            isLiked: false,
            isDisliked: false
        }
    }

    async likeComment(id: string, req: Request): Promise<ILikesChanged> {
        try {
            const accessToken = req.headers.authorization as string;
            const {userId} = this.authService.checkToken(accessToken)
            const currentComment = await this.commentsRepository.findOne({where: {id: +id}})
            if (!currentComment) {throw new Error('Нет данного комментария в бд')}
            const isDisliked = await this.checkIsLikedOrDislikedComment(userId, +id, 'dislikes')
            if (isDisliked) {
                await this.deleteLikeOrDislikeCommentFromDB(currentComment, userId, 'dislikes')
            }
            await this.addLikeOrDislikeCommentFromDB(currentComment, userId, 'likes')
            const savedComment = await this.commentsRepository.findOne({where: {id: +id}})

            return {
                likes: savedComment?.likes.length || 0,
                dislikes: savedComment?.dislikes.length || 0,
                isLiked: true,
                isDisliked: false
            }
        } catch (err) {
            throw new ServiceUnavailableException('Что-то пошло не так')
        }
    }

    async deleteLikeComment(id: string, req: Request): Promise<ILikesChanged> {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken)
        const currentComment = await this.commentsRepository.findOne({where: {id: +id}})
        if (!currentComment) {throw new BadRequestException('Нет комментария')}

        await this.deleteLikeOrDislikeCommentFromDB(currentComment, userId, 'likes')

        const savedComment = await this.commentsRepository.findOne({where: {id: +id}})

        return {
            likes: savedComment?.likes.length || 0,
            dislikes: savedComment?.dislikes.length || 0,
            isLiked: false,
            isDisliked: false
        }
    }

    async dislikeComment(id: string, req: Request): Promise<ILikesChanged> {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken)
        const currentComment = await this.commentsRepository.findOne({where: {id: +id}})

        if (!currentComment) {throw new BadRequestException('Нет видео')}
        const isLiked = await this.checkIsLikedOrDislikedComment(userId, +id, 'likes')

        if (isLiked) {
            await this.deleteLikeOrDislikeCommentFromDB(currentComment, userId, 'likes')
        }

        await this.addLikeOrDislikeCommentFromDB(currentComment, userId, 'dislikes')
        const savedComment = await this.commentsRepository.findOne({where: {id: +id}})

        return {
            likes: savedComment?.likes.length || 0,
            dislikes: savedComment?.dislikes.length || 0,
            isLiked: false,
            isDisliked: true
        }
    }

    async deleteDislikeComment(id: string, req: Request): Promise<ILikesChanged> {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken)
        const currentComment = await this.commentsRepository.findOne({where: {id: +id}})
        if (!currentComment) {throw new BadRequestException('Нет комментария')}

        await this.deleteLikeOrDislikeCommentFromDB(currentComment, userId, 'dislikes')

        const savedComment = await this.commentsRepository.findOne({where: {id: +id}})


        return {
            likes: savedComment?.likes.length || 0,
            dislikes: savedComment?.dislikes.length || 0,
            isLiked: false,
            isDisliked: false
        }
    }
}
