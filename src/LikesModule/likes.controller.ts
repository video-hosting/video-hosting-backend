import {
    Controller,
    Req,
    UseGuards,
    Get,
    Param,
    Delete,
} from '@nestjs/common';
import {Request} from 'express'
import {LikesService} from "./likes.service";
import {AuthGuard} from "../core/guards/auth.guard";

@Controller('api/likes')
@UseGuards(AuthGuard)
export class LikesController {
    constructor(private readonly likesService: LikesService) {}

    @Get('/like/:id')
    async likeVideo(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.likeVideo(id, req)
    }

    @Delete('/like/:id')
    async deleteLikeVideo(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.deleteLikeVideo(id, req)
    }

    @Get('/dislike/:id')
    async dislikeVideo(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.dislikeVideo(id, req)
    }

    @Delete('/dislike/:id')
    async deleteDislikeVideo(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.deleteDislikeVideo(id, req)
    }

    @Get('/like-comment/:id')
    async likeComment(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.likeComment(id, req)
    }

    @Delete('/like-comment/:id')
    async deleteLikeComment(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.deleteLikeComment(id, req)
    }

    @Get('/dislike-comment/:id')
    async dislikeComment(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.dislikeComment(id, req)
    }

    @Delete('/dislike-comment/:id')
    async deleteDislikeComment(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.likesService.deleteDislikeComment(id, req)
    }

}
