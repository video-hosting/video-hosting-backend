import {Injectable} from '@nestjs/common';
import {Request} from "express";
import {AuthService} from "../AuthModule/auth.service";
import {UsersService} from "../UsersModule/users.service";
import {VideoEntity} from "../VideoModule/entities/video.entity";

@Injectable()
export class ViewsService {
    constructor(
        private readonly authService: AuthService,
        private readonly userService: UsersService,
    ) {}

    async setViewVideo(id: string, req: Request) {
        const accessToken = req.headers.authorization as string;
        const {userId} = this.authService.checkToken(accessToken);
        const user = await this.userService.findUserById(userId);
        await this.userService.update({
            ...user,
            views: [...user.views, {id: +id}] as VideoEntity[]
        })

    }
}
