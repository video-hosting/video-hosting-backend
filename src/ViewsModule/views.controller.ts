import {
    Controller,
    Req,
    UseGuards,
    Get,
    Param,
} from '@nestjs/common';
import {Request} from 'express'
import {ViewsService} from "./views.service";
import {AuthGuard} from "../core/guards/auth.guard";

@Controller('api/views')
@UseGuards(AuthGuard)
export class ViewsController {
    constructor(private readonly viewsService: ViewsService) {}

    @Get(':id')
    async setViewVideo(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.viewsService.setViewVideo(id, req)
    }
}
