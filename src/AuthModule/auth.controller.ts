import {
    Controller,
    Post,
    Get,
    Body,
    Req,
    Res,
    Query,
    Put
} from '@nestjs/common';
import {Response, Request} from 'express'
import {AuthService} from "./auth.service";
import {CreateUserDto} from "../UsersModule/dto/createUser.dto";
import {LoginDto} from "./dto/login.dto";
import {UpdatePassDto} from "./dto/updatePass.dto";

@Controller('api')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
    ) {}

    @Post('signup')
    async signup(
        @Req() request: Request,
        @Res({passthrough: true}) response: Response,
        @Body() createUser: CreateUserDto
    ) {
        const {
            user,
            accessToken,
            refreshToken
        } = await this.authService.signup(createUser, request.ip as string);

        this.authService.setAccessTokenToCookie(response, accessToken)
        this.authService.setRefreshTokenToCookie(response, refreshToken)

        return {...user, accessToken: accessToken}
    }
    @Get('confirm-account')
    async confirmAccount(
        @Query('key') key: string,
        @Res({passthrough: true}) response: Response,
    ){
        const {userInfo, accessToken, refreshToken} = await this.authService.confirmAccount(key)

        this.authService.setAccessTokenToCookie(response, accessToken)
        this.authService.setRefreshTokenToCookie(response, refreshToken)

        return userInfo
    }

    @Get('update-access-token')
    async updateAccessToken(
        @Req() request: Request,
        @Res() response: Response,
    ){
        const {refreshToken} = request.cookies

        if (!refreshToken) {
            response.status(401)
            response.end()
        }
        const {userId, iat, exp} = this.authService.checkToken(refreshToken)
        if (exp < iat) {
            response.status(401)
            response.end()
        }

        this.authService.setAccessTokenToCookie(response, this.authService.getAccessToken(userId))

        response.end()
    }

    @Post('sign-in')
    async signIn(
        @Body() userData: LoginDto,
        @Req() request: Request,
        @Res() response: Response,
    ) {
        const {refreshToken} = request.cookies;
        if (!refreshToken && !userData.login) {
            response.status(401)
            response.end()
            return
        } else if (refreshToken) {
            const res = await this.authService.signInByRefreshToken(refreshToken)
            if (res) {
                this.authService.setAccessTokenToCookie(response, res.accessToken)

                response.status(200)
                response.json({...res.user, accessToken: res.accessToken})
            } else {
                this.authService.clearCookies(response)
                response.status(401)
            }
            response.end()
            return
        } else {
            const res = await this.authService.signInByEmailOrLogin(userData.login, userData.passwordHash)
            if (!res) {
                response.status(401)
                response.json({message: 'Неверные логин или пароль'})
                response.end()
            } else {
                this.authService.setAccessTokenToCookie(response, res.accessToken)
                this.authService.setRefreshTokenToCookie(response, res.refreshToken)
                response.status(200)
                response.json({...res.user, accessToken: res.accessToken})
                response.end()
            }
        }
    }

    @Get('logout')
    async logout(
        @Res({passthrough: true}) response: Response,
    ) {
        this.authService.clearCookies(response)
        return
    }

    @Get('reset-password')
    async resetPassword (
        @Query('login') login: string,
        @Res() response: Response,
    ) {
        const res = await this.authService.resetPassword(login)
        if (!res) {
            response.status(400)
            response.end()
        } else {
            response.status(200)
            response.end()
        }
    }
    @Get('check-link-reset-password')
    async checkLinkResetPassword(
        @Query('key') key: string,
    ) {
        return await this.authService.checkLinkResetPassword(key)
    }

    @Put('update-password')
    async updatePassword(
        @Req() request: Request,
        @Body() passData: UpdatePassDto
    ) {
        return await this.authService.updatePassword(passData, request.ip as string)
    }
}
