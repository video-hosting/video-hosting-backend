import {IsString} from 'class-validator';

export class UpdatePassDto {
    @IsString()
    passwordHash: string;
    @IsString()
    key: string;
}
