import {IsString, IsNumber} from 'class-validator';

export class ResetPasswordDto {
    @IsString()
    id: string;
    @IsNumber()
    userId: number;
}
