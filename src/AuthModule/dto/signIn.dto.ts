import {IsString} from 'class-validator';

export class SignInDto {
    @IsString()
    user: string;
    @IsString()
    passwordHash: string;
}
