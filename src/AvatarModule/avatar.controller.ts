import {Controller, Get, Param, Req, Res} from '@nestjs/common';
import {Request, Response} from "express";
import * as path from "path";
import * as fs from "fs";
import {AvatarService} from "./avatar.service";
import {SAVE_FILES_PATH} from "../core/constants";

@Controller('api')
export class AvatarController {
    constructor(
        private readonly avatarService: AvatarService,
    ) {}

    @Get('avatar/:userId')
    async getAvatar(
        @Param('userId') userId: string,
        @Req() request: Request,
        @Res() response: Response,
    ) {
        const isHaveAvatar = fs.existsSync(path.resolve(SAVE_FILES_PATH, userId.toString(), `avatar.webp`))
        if (!isHaveAvatar) {
            response.sendFile(path.resolve('src', 'core', 'images', 'defaultAvatar.webp'))
            response.set({'Cache-Control': 'private, max-age=0'});
        } else {
            response.sendFile(path.resolve(SAVE_FILES_PATH, userId.toString(), `avatar.webp`))
            response.set({'Cache-Control': 'private, max-age=3600'});
        }
    }
}
