import {
    Controller,
    Req,
    Get,
    Param,
    Post,
    Body,
} from '@nestjs/common';
import {Request} from 'express'
import {ChannelsService} from "./channels.service";

@Controller('api/channel')
export class ChannelsController {
    constructor(private readonly commentsService: ChannelsService) {}
    @Get(':id')
    async getChannelVideos (
        @Param('id') channelId: string,
        @Req() req: Request,
    ) {
        return await this.commentsService.getChannelInfo(channelId, req)
    }

}
