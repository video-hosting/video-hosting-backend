import {BadRequestException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {Repository} from "typeorm";
import {AuthService} from "../AuthModule/auth.service";
import {Request} from "express";
import {User} from "../UsersModule/entities/user.entity";
import {prepareUser, prepareVideo} from "../core/utils/prepareModels";

@Injectable()
export class ChannelsService {
    constructor(
        @InjectRepository(User as EntityClassOrSchema)
        private readonly usersRepository: Repository<User>,
        private readonly authService: AuthService,
    ) {}

    async getChannelInfo (channelId: string, req: Request) {
        let isMyChannel = false
        if (req?.headers?.authorization) {
            try {
                const { userId} = this.authService.checkToken(req.headers.authorization)
                if (userId === +userId) {
                    isMyChannel = true
                }
            } catch (err){}
        }

        try {
            const user = await this.usersRepository.findOneOrFail({
                where: {id: +channelId},
                relations: ['subscribers']
            })

            const videos = await this.usersRepository
                .createQueryBuilder()
                .relation(User, 'videos')
                .of(user)
                .loadMany()
            const userRes = prepareUser(user)
            const videosRes = videos
                .filter((v) => !v.isDeleted)
                .map((v) => prepareVideo(v))
                .filter((v) => {
                    if (isMyChannel) return true
                    return !v.isPrivate
                })

            return {...userRes, subscribersCount: user.subscribers?.length || 0, videos: videosRes}
        } catch (err) {
            throw new BadRequestException('Не удалось получить видео канала')
        }
    }
}
