import {forwardRef, Module} from '@nestjs/common';
import {ConfigModule, ConfigService} from "@nestjs/config";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../UsersModule/entities/user.entity";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {AuthService} from "../AuthModule/auth.service";
import {ResetPasswordEntity} from "../AuthModule/entities/resetPassword.entity";
import {MailService} from "../MailModule/mail.service";
import {AuthModule} from "../AuthModule/auth.module";
import {UsersService} from "../UsersModule/users.service";
import {VideoEntity} from "../VideoModule/entities/video.entity";
import {ChannelsController} from "./channels.controller";
import {ChannelsService} from "./channels.service";

@Module({
    imports: [
        forwardRef(() => AuthModule),
        TypeOrmModule.forFeature([User, VideoEntity, ResetPasswordEntity] as EntityClassOrSchema[]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (config: ConfigService) => ({
                secret: config.get('secretKey'),
                signOptions: { expiresIn: config.get('expiresIn') },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [ChannelsController],
    providers: [ChannelsService, AuthService, MailService, JwtService, UsersService, ConfigService],
})
export class ChannelsModule {}
