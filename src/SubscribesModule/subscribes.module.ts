import {forwardRef, Module} from '@nestjs/common';
import {ConfigModule, ConfigService} from "@nestjs/config";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../UsersModule/entities/user.entity";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {AuthService} from "../AuthModule/auth.service";
import {ResetPasswordEntity} from "../AuthModule/entities/resetPassword.entity";
import {MailService} from "../MailModule/mail.service";
import {AuthModule} from "../AuthModule/auth.module";
import {UsersService} from "../UsersModule/users.service";
import {VideoEntity} from "../VideoModule/entities/video.entity";
import {VideoService} from "../VideoModule/video.service";
import {SubscribesController} from "./subscribes.controller";
import {VideoModule} from "../VideoModule/video.module";
import {SubscribesService} from "./subscribes.service";
import {VideoCommentEntity} from "../CommentsModule/entities/videoComment.entity";
import {ViewStatsEntity} from "../VideoModule/entities/viewStats.entity";

@Module({
    imports: [
        forwardRef(() => AuthModule),
        forwardRef(() => VideoModule),
        TypeOrmModule.forFeature([User, VideoEntity, ResetPasswordEntity, VideoCommentEntity, ViewStatsEntity] as EntityClassOrSchema[]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (config: ConfigService) => ({
                secret: config.get('secretKey'),
                signOptions: { expiresIn: config.get('expiresIn') },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [SubscribesController],
    providers: [SubscribesService, VideoService, AuthService, MailService, JwtService, UsersService, ConfigService],
})
export class SubscribesModule {}
