import {
    Controller,
    Req,
    UseGuards,
    Get,
    Param,
    Delete,
} from '@nestjs/common';
import {Request} from 'express'
import {SubscribesService} from "./subscribes.service";
import {AuthGuard} from "../core/guards/auth.guard";

@Controller('api/subscribes')
@UseGuards(AuthGuard)
export class SubscribesController {
    constructor(private readonly subscribesService: SubscribesService) {}

    @Get('/subscribe/:id')
    async subscribe(
        @Req() req: Request,
        @Param('id') userId: string
    ) {
        return this.subscribesService.subscribe(userId, req)
    }

    @Delete('/subscribe/:id')
    async unsubscribe(
        @Req() req: Request,
        @Param('id') id: string
    ) {
        return this.subscribesService.unsubscribe(id, req)
    }
}
