import {Injectable, ServiceUnavailableException} from '@nestjs/common';
import {Request} from "express";
import {AuthService} from "../AuthModule/auth.service";
import {UsersService} from "../UsersModule/users.service";
import {User} from "../UsersModule/entities/user.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {Repository} from "typeorm";

@Injectable()
export class SubscribesService {
    constructor(
        @InjectRepository(User as EntityClassOrSchema)
        private readonly usersRepository: Repository<User>,
        private readonly authService: AuthService,
        private readonly userService: UsersService,
    ) {}

    async subscribe(id: string, req: Request) {
        try {
            const accessToken = req.headers.authorization as string;
            const {userId} = this.authService.checkToken(accessToken)
            const subscriber = await this.userService.findUserById(userId, [])

            await this.usersRepository
                .createQueryBuilder()
                .relation(User, 'subscribes')
                .of(subscriber)
                .add(+id)

        } catch (err) {
            throw new ServiceUnavailableException('Что-то пошло не так')
        }
    }

    async unsubscribe(id: string, req: Request) {
        try {
            const accessToken = req.headers.authorization as string;
            const {userId} = this.authService.checkToken(accessToken)
            const subscriber = await this.userService.findUserById(userId, [])
            await this.usersRepository
                .createQueryBuilder()
                .relation(User, 'subscribes')
                .of(subscriber)
                .remove(+id)
        } catch (err) {
            throw new ServiceUnavailableException('Что-то пошло не так')
        }
    }
}
