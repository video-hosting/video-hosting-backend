import * as ffmpeg from 'fluent-ffmpeg';
import * as path from "path";
import * as fs from "fs";
import {FfmpegCommand, FfprobeData} from "fluent-ffmpeg";
import {
    SAVE_FILES_PATH, SCREENSHOTS_NAME,
    VIDEO_FILE_1280_MP4_NAME,
    VIDEO_FILE_1920_MP4_NAME, VIDEO_FILE_360_MP4_NAME,
    VIDEO_FILE_480_MP4_NAME
} from "../constants";
import * as sharp from "sharp";

const getCodecInfoPromise = (filePath: string): Promise<FfprobeData> => new Promise((res, rej) => {
    ffmpeg.ffprobe(filePath, function (err, metadata) {
        if (err) {
            rej(err)
        }
        if (metadata) {
            res(metadata)
        }
    })
})

export class CodecInstance {
    command: FfmpegCommand

    constructor(filePath: string) {
        this.command = ffmpeg(filePath)
    }

    private async saveVideo (filePath: string, size: string) {
        return await new Promise((res, rej) => {
            this.command
                .videoCodec('libx264')
                .size(size)
                .aspect("16:9")
                .autopad()
                .on('error', function (err) {
                    rej('An error occurred: ' + err.message);
                })
                .on('end', function () {
                    res('Processing finished !');
                })
                .save(filePath);
        })
    }

    async saveHDVideo(userId: number, videoId: string) {
        return await this.saveVideo(
            path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos', videoId, VIDEO_FILE_1280_MP4_NAME),
            '1280x720'
        )
    }

    async saveFullHDVideo(userId: number, videoId: string) {
        return await this.saveVideo(
            path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos', videoId, VIDEO_FILE_1920_MP4_NAME),
            '1920x1080'
        )
    }

    async save480pVideo(userId: number, videoId: string) {
        return await this.saveVideo(
            path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos', videoId, VIDEO_FILE_480_MP4_NAME),
            '854x480'
        )
    }

    async save360pVideo(userId: number, videoId: string) {
        return await this.saveVideo(
            path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos', videoId, VIDEO_FILE_360_MP4_NAME),
            '640x360'
        )
    }

    async saveScreenshots (userId: number, videoId: string) {
        const screenshotsPath = path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos', videoId, SCREENSHOTS_NAME)

        await new Promise((res, rej) => {
            this.command
                .on('error', function (err) {
                    rej('An error occurred: ' + err.message);
                })
                .on('end', function () {
                    res('Processing finished !');
                })
                .screenshots({
                    count: 4,
                    folder: screenshotsPath,
                    size: '640x360'
                })
        })
        fs.readdir(screenshotsPath, (err, files) => {
            files.forEach(async file => {
                const f = fs.readFileSync(path.resolve(screenshotsPath, file))
                await sharp(f)
                    .resize(160, 90, {fit: "cover"})
                    .webp({effort: 3})
                    .toFile(path.resolve(screenshotsPath, `${file.slice(0, file.length - 3)}webp`))
                if (file !== 'tn_1.png') {
                    fs.unlinkSync(path.resolve(screenshotsPath, file))
                }
            });
        });
    }

    static async getCodecInfo(filePath: string) {
        const metadata = await getCodecInfoPromise(filePath)
        const [videoCodec, audioCodec] = metadata.streams
        const height = videoCodec.height
        const width = videoCodec.width
        const duration = metadata.format.duration
        const videoCodecName = videoCodec.codec_name
        return {
            height, width, duration, videoCodecName
        }
    }
}