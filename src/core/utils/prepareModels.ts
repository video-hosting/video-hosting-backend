import {User} from "../../UsersModule/entities/user.entity";
import {IUserResponse} from "../types/IUserResponse";
import {VideoEntity} from "../../VideoModule/entities/video.entity";
import {IVideoResponse} from "../types/IVideoResponse";
import {IUserForLoginResponse} from "../types/IUserForLoginResponse";
import {VideoCommentEntity} from "../../CommentsModule/entities/videoComment.entity";
import {IChildCommentResponse, ICommentResponse} from "../types/ICommentResponse";
import {VideoChildCommentEntity} from "../../CommentsModule/entities/videoChildComment.entity";

const getDate = (d: Date) => {
    const x = new Date();
    const offset= -x.getTimezoneOffset();
    const hours = +(parseInt((offset/60).toString())+"."+String(offset%60).padStart(2, "0"))

    const date = new Date(d)
    date.setHours(d.getHours() + hours)
    return date
}

export const prepareVideo = (v: VideoEntity, user?: User):IVideoResponse => {
    return {
        id: v.id,
        likes: v.likes?.length || 0,
        dislikes: v.dislikes?.length || 0,
        name: v.name,
        createdAt: getDate(v.createdAt),
        viewsCount: v.viewsCount,
        description: v.description,
        duration: v.duration,
        tags: v.tags,
        comments: v.comments ? v.comments.map((c) => prepareComment(c, user)) : [],
        ...(v.user && {user: prepareUser(v.user)}),
        ...(v.views && {views: v.views.length}),
        ...(v.isPrivate && {isPrivate: v.isPrivate}),
        isAgeRelatedContent: v.isAgeRelatedContent,
        isModerating: v.isModerating,
    }
}

export const prepareUser = (u: User): IUserResponse => {
    return {
        id: u.id,
        nickname: u.nickname,
        videos: u.videos ? u.videos.map((v) => prepareVideo(v)) : []
    }
}

export const prepareUserForLogin = (u: User): IUserForLoginResponse => {
    return {
        id: u.id,
        login: u.login,
        email: u.email,
        name: u.name,
        nickname: u.nickname,
        surname: u.surname,
        accountIsConfirm: u.accountIsConfirm,
        subscribes: u.subscribes?.map(prepareUser) || [],
        role: u.role
    }
}

export const prepareComment = (c:VideoCommentEntity, user?: User): ICommentResponse => {
    return {
        id: c.id,
        createdAt: getDate(c.createdAt),
        text: c.text,
        user: c.user ? prepareUser(c.user) : c.user,
        likesCount: c.likes?.length || 0,
        dislikesCount: c.dislikes?.length || 0,
        isLiked: user ? c.likes.some(u => u.id === user.id) : false,
        isDisliked: user ? c.dislikes.some(u => u.id === user.id) : false,
        childrenCommentsCount: c.childrenComments?.length || 0
    }
}

export const prepareChildComment = (c: VideoChildCommentEntity): IChildCommentResponse => {
    return {
        id: c.id,
        createdAt: getDate(c.createdAt),
        text: c.text,
        replyId: c.replyId || undefined,
        user: c.user ? prepareUser(c.user) : c.user,
        answerUser: c.answerUser ? prepareUser(c.answerUser) : c.answerUser,
    }
}