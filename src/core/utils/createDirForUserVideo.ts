import * as fs from 'fs'
import * as path from "path";
import {ServiceUnavailableException} from "@nestjs/common";
import {SAVE_FILES_PATH, VIDEO_FILE_MP4_NAME} from "../constants";

export const createDirForUserVideo = (userId: number, videoId: string) => {
    const isHaveUserDir = fs.existsSync(path.resolve(SAVE_FILES_PATH, userId.toString()))
    if (!isHaveUserDir) {
        try {
            fs.mkdirSync(path.resolve(SAVE_FILES_PATH, userId.toString()))
        } catch (err) {
            throw new ServiceUnavailableException('Не удалось создать папку пользователя')
        }
    }
    const isHaveUserVideosDir = fs.existsSync(path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos'))

    if (!isHaveUserVideosDir) {
        try {
            fs.mkdirSync(path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos'))
        } catch (err) {
            throw new ServiceUnavailableException('Не удалось создать папку с видео пользователя')
        }
    }

    const isHaveCurrentVideosDir = fs.existsSync(path.resolve(
        SAVE_FILES_PATH, userId.toString(), 'videos', videoId, VIDEO_FILE_MP4_NAME))

    if (isHaveCurrentVideosDir) {
        throw new ServiceUnavailableException('Файл уже загружен')
    }
    try {
        fs.mkdirSync(path.resolve(SAVE_FILES_PATH, userId.toString(), 'videos', videoId))
    } catch (err) {
        throw new ServiceUnavailableException('Не удалось создать папку для видео')
    }
}