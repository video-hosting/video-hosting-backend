class ViewsTimerControl {
    constructor() {
        // @ts-ignore
        if (!global.views) {global.views = {}}
    }
    checkTimer (ip: string | string[] | undefined, videoId: number):boolean {
        if (!ip) return true
        // @ts-ignore
        return !global.views[`${ip}___${videoId}`]
    }

    setTimer (ip: string | string[] | undefined, videoId: number) {
        if (this.checkTimer(ip, videoId)) {
            // @ts-ignore
            global.views[`${ip}___${videoId}`] = true

            setTimeout(() => {
                // @ts-ignore
                delete global.views[ip]
            }, 1000 * 60)
        }
    }
}

export const viewsTimerControl = new ViewsTimerControl()