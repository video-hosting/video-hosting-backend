import { IsNumber, IsDate, IsNotEmpty, IsPositive } from 'class-validator';
import {
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
} from 'typeorm';

export class EntityBase {
  @PrimaryGeneratedColumn()
  @IsNumber()
  @IsPositive()
  @IsNotEmpty()
  id: number;
  @CreateDateColumn()
  @IsDate()
  createdAt: Date;
  @UpdateDateColumn()
  @IsDate()
  updateAt: Date;
}
