export const checkEmail = (email: string) => {
    return (/\S+@\S+\.\S+/).test(email)
}