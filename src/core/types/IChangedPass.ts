export interface IChangedPass {
    time: string;
    oldPassHash: string;
    ip: string;
}