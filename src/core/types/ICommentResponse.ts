import {IUserResponse} from "./IUserResponse";

export interface ICommentResponse {
    id: number;
    createdAt: Date;
    text: string;
    user: IUserResponse;
    childrenCommentsCount: number;
    likesCount: number;
    dislikesCount: number;
    isLiked: boolean;
    isDisliked: boolean;
}

export interface IChildCommentResponse {
    id: number;
    createdAt: Date;
    text: string;
    replyId?: number;
    user: IUserResponse;
    answerUser: IUserResponse | null;
}