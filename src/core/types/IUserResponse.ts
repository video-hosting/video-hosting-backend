interface IVideoInUser {
    name: string;
    description: string;
    duration: number;
    tags: string[];
}

export interface IUserResponse {
    id: number;
    nickname: string;
    videos: IVideoInUser[];
}