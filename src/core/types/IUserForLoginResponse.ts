import {Role} from "../constants";
import {IUserResponse} from "./IUserResponse";

export interface IUserForLoginResponse {
    id: number;
    login: string;
    email: string;
    name: string;
    nickname: string;
    surname: string;
    accountIsConfirm: boolean;
    subscribes: IUserResponse[];
    role: Role[]
}