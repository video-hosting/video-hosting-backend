import {ICommentResponse} from "./ICommentResponse";

interface IUserInVideo {
    id: number;
    nickname: string;
}

export interface IVideoResponse {
    id: number;
    createdAt: Date;
    name: string;
    description: string;
    likes: number;
    dislikes: number;
    viewsCount: number;
    views?: number;
    user: IUserInVideo;
    duration: number;
    tags: string[];
    isPrivate?: boolean;
    isAgeRelatedContent: boolean;
    isModerating: boolean;
    comments: ICommentResponse[];
}

export interface IVideoResponseWithUserLikes extends IVideoResponse {
    isLikedVideo?: boolean;
    isDislikedVideo?: boolean;
    user: IVideoResponse['user'] & {subscribersCount: number}
}

export interface ILikesChanged {
    likes: number;
    dislikes: number;
    isLiked: boolean;
    isDisliked: boolean;
}