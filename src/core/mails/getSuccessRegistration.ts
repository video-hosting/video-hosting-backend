export const getSuccessRegistration = (
    email: string,
    nickname: string,
    name: string,
    surname: string,
) => `<section>
        <h1>Вы успешно зарегестрировались</h1>
        <p>Регистрационные данные вашего аккаунта:</p>
        <p>Email: ${email}</p>
        <p>Логин: ${nickname}</p>
        <p>Имя: ${name}</p>
        <p>Фамилия: ${surname}</p>
</section>`;
