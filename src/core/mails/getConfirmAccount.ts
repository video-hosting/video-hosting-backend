export const getConfirmAccount = (login:string, confirmURL: string) => `<section>
        <h1>Подтверждаение аккаунта</h1>
        <p>Для подтверждения аккаунта (логин - ${login}) перейдите  <a href=${confirmURL}>по ссылке</a></p>
</section>`;