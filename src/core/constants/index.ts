export const SAVE_FILES_PATH = process.env.FILES_DIRECTORY || 'files';

export const VIDEO_FILE_MP4_NAME = 'video.mp4'
export const VIDEO_FILE_1920_MP4_NAME = 'video1920.mp4'
export const VIDEO_FILE_1280_MP4_NAME = 'video1280.mp4'
export const VIDEO_FILE_480_MP4_NAME = 'video480.mp4'
export const VIDEO_FILE_360_MP4_NAME = 'video360.mp4'
export const SCREENSHOTS_NAME = 'screenshots'

export enum PostgresErrorCode {
    UniqueViolation = '23505',
    CheckViolation = '23514',
    NotNullViolation = '23502',
    ForeignKeyViolation = '23503',
}

export enum Role {
    user = 'user',
    admin = 'admin',
}

export const enum loginLength {
    min = 1,
    max = 20,
}

export type MailServiceData = {
    from: { name: string; address: string };
    subject: string;
    html: string;
    to: string;
}

export const enum Qualities {
    p1080 = '1080',
    p720 = '720',
    p480 = '480',
    p360 = '360',
}