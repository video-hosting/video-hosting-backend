import {BadRequestException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {FindOptionsRelationByString, FindOptionsRelations, Repository} from "typeorm";
import * as sharp from "sharp";
import * as fs from "fs";
import * as path from "path";
import {Response} from "express";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {User} from "./entities/user.entity";

import {SAVE_FILES_PATH} from "../core/constants";
import {prepareUserForLogin} from "../core/utils/prepareModels";


@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User as EntityClassOrSchema)
        private readonly usersRepository: Repository<User>,
    ) {
    }

    async removeUser(userId: number) {
        const user = await this.usersRepository.findOneOrFail({
            where: {id: userId},
        });
        await this.usersRepository.remove(user);
    }

    async getAllUsers() {
        return await this.usersRepository
            .createQueryBuilder()
            .getMany()
    }

    async findUserByEmail(email: string) {
        return await this.usersRepository.findOne({
            where: {email},
            relations: ['subscribes']
        });
    }

    async findUserByLogin(login: string) {
        return await this.usersRepository.findOne({
            where: {login},
            relations: ['subscribes']
        });
    }

    async findUserByIdForLogin(id: number) {
        if (!id) throw new BadRequestException('Пользователь не найден');
        const user = await this.usersRepository
            .findOne({
                where: {id},
                relations: ['subscribes']
            });
        if (!user) {
            throw new BadRequestException('Пользователь не найден');
        } else {
            return prepareUserForLogin(user)
        }
    }

    async findUserById(id: number, relations?: FindOptionsRelations<User> | FindOptionsRelationByString) {
        if (!id) throw new BadRequestException('Пользователь не найден');
        const user = await this.usersRepository
            .findOne({
                where: {id},
                relations: relations || [
                    'videos',
                    'likes',
                    'dislikes',
                    'likesComments',
                    'dislikesComments',
                    'subscribers',
                    'subscribes',
                    'views'
                ]
            });
        if (!user) {
            throw new BadRequestException('Пользователь не найден');
        } else {
            return user
        }
    }

    async create(createUserDto: User) {
        return await this.usersRepository.save(createUserDto);
    }

    async update(updateUser: User) {
        return await this.usersRepository.save(updateUser)
    }

    async uploadAvatar(userId: number, response: Response, file: Express.Multer.File) {
        const isHaveUserDir = fs.existsSync(path.resolve(SAVE_FILES_PATH, userId.toString()))
        if (!isHaveUserDir) {
            fs.mkdir(path.resolve(SAVE_FILES_PATH, userId.toString()), (err: any) => {
                if (err) {
                    response.status(500)
                    response.json({message: 'не удалось создать папку пользователя'})
                    response.end()
                }
            })
        }
        await sharp(file.buffer)
            .resize(200, 200, {fit: "cover"})
            .webp({effort: 3})
            .toFile(path.resolve(SAVE_FILES_PATH, userId.toString(), `avatar.webp`))
        response.status(200)
        response.end()
    }
}
