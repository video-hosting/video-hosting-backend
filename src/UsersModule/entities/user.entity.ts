import {Column, Entity, JoinTable, ManyToMany, OneToMany} from 'typeorm';
import {IsArray, IsBoolean, IsString, Length, IsOptional} from 'class-validator';
import {EntityBase} from "../../core/utils/entity_base";
import {loginLength, Role} from "../../core/constants";
import {IChangedPass} from "../../core/types/IChangedPass";
import {VideoEntity} from "../../VideoModule/entities/video.entity";
import {VideoCommentEntity} from "../../CommentsModule/entities/videoComment.entity";
import {VideoChildCommentEntity} from "../../CommentsModule/entities/videoChildComment.entity";

@Entity()
export class User extends EntityBase {
  @Column()
  @IsString()
  @Length(loginLength.min, loginLength.max)
  login: string;

  @Column()
  @IsString()
  email: string;

  @Column()
  @IsString()
  nickname: string;

  @Column()
  @IsString()
  @IsOptional()
  registerIp: string;

  @Column()
  @IsString()
  @Length(loginLength.min, loginLength.max)
  name: string;

  @Column()
  @IsString()
  @Length(loginLength.min, loginLength.max)
  surname: string;

  @Column()
  @IsString()
  @IsOptional()
  passwordHash: string;

  @Column({ default: false })
  @IsBoolean()
  accountIsConfirm: boolean;

  @Column({ type: 'text', array: true, default: [Role.user] })
  @IsArray()
  role: Role[];

  @Column({ type: 'text', array: true, default: [] })
  @IsArray()
  changedPass: IChangedPass[];

  @OneToMany(() => VideoEntity, (d) => d.user)
  videos: VideoEntity[];

  @ManyToMany(() => VideoEntity, (d) => d.views)
  views: VideoEntity[];

  @ManyToMany(() => VideoEntity, (video) => video.likes)
  likes: VideoEntity[];

  @ManyToMany(() => VideoEntity, (video) => video.dislikes)
  dislikes: VideoEntity[];

  @OneToMany(() => VideoCommentEntity, (c) => c.user)
  comments: VideoCommentEntity[];

  @OneToMany(() => VideoChildCommentEntity, (c) => c.answerUser)
  answerComments: VideoChildCommentEntity[];

  @ManyToMany(() => VideoCommentEntity, (c) => c.likes)
  likesComments: VideoCommentEntity[];

  @ManyToMany(() => VideoCommentEntity, (c) => c.dislikes)
  dislikesComments: VideoCommentEntity[];

  @ManyToMany(() => User, c => c.subscribes)
  @JoinTable({name: "subscribers"})
  subscribers: User[]

  @ManyToMany(() => User, c => c.subscribers)
  subscribes: User[]
}
