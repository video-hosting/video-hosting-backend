import {forwardRef, Module} from '@nestjs/common';
import {JwtModule} from "@nestjs/jwt";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {TypeOrmModule} from "@nestjs/typeorm";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {UsersService} from './users.service';
import {UsersController} from './users.controller';
import {User} from "./entities/user.entity";
import {AuthModule} from "../AuthModule/auth.module";
import {AuthService} from "../AuthModule/auth.service";
import {ResetPasswordEntity} from "../AuthModule/entities/resetPassword.entity";
import {MailService} from "../MailModule/mail.service";

@Module({
    providers: [UsersService, AuthService, MailService, ConfigService],
    imports: [
        forwardRef(() => AuthModule),
        TypeOrmModule.forFeature([User, ResetPasswordEntity] as EntityClassOrSchema[]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (config: ConfigService) => ({
                secret: config.get('secretKey'),
                signOptions: { expiresIn: config.get('expiresIn') },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [UsersController],
    exports: [UsersService],
})
export class UsersModule {
}
