import {IsString, IsOptional} from 'class-validator';

export class CreateUserDto {
    @IsString()
    login: string;
    @IsString()
    email: string;
    @IsString()
    name: string;
    @IsString()
    surname: string;
    @IsString()
    @IsOptional()
    passwordHash: string;
}
